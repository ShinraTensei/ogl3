#include "engine.hpp"
#include "shader.hpp"

int main()
{
    Engine e("GLSL Shoting", 800, 600);

    Shader shader("res/practiceShader");

    while(e.running())
    {
        shader.Bind();

        glBegin(GL_QUADS);
            glVertex3f(0.0f, 0.0f, 0.0f);
            glVertex3f(1.0f, 0.0f, 0.0f);
            glVertex3f(1.0f, 1.0f, 0.0f);
            glVertex3f(0.0f, 1.0f, 0.0f);
        glEnd();

        e.update();
    }

    return 0;
}
