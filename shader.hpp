#ifndef SHADER_HPP
#define SHADER_HPP

#include <GL/glew.h>
#include <GL/gl.h>
#include <string>
#include <iostream>
#include <fstream>

class Shader
{
public:
    Shader(const std::string& fileName);

    void Bind();

    virtual ~Shader();
private:
    static const unsigned int NUM_SHADERS = 2;

    Shader(const Shader& other) {}
    void operator=(const Shader& other) {}

    GLuint m_program;
    GLuint m_shaders[NUM_SHADERS];
};

#endif // SHADER_HPP
