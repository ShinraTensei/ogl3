#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

class Engine
{
public:
    Engine(const char* windowTitle, int windowWidth, int windowHeight);

    bool running();
    void update();

private:
    SDL_Window *window;
    SDL_GLContext glc;
};

#endif // ENGINE_HPP
