#ifndef MESH_HPP
#define MESH_HPP

class Vertex
{
public:
protected:
private:
};

class Mesh
{
public:
    Mesh();

    void Draw();

    virtual ~Mesh();

private:
    Mesh(const Mesh &other);
    void operator=(const Mesh& other);
};

#endif // MESH_HPP
